// © 2020 - 2023 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

// =============================================================================================
// Memory Map:
// =============================================================================================
// Device ID string, word 1
// Device ID string, word 2
// Device version
// System clock frequency (32 bits)
// Slave ID / address / data length register, {1'b0, slave_id, address, 6'b0, data_length}
// Control register, {ipoll_enable_slave_id, 3'b0, enable_ipoll, 2'b0, enable_irq, enable_relative_address, enable_extended_address_mode, enable_crc_protection, enhanced_error_recovery, internal_cmd_issue_delay, 6'b0, data_direction, start_cycle}
// Status register, {debug_port, 8'b0, ipoll_error, 4'b0, cycle_error, 7'b0, cycle_complete}
// TX data (32 bits)
// RX data (32 bits)
// IRQ status / DMA control register, {4'b0, dma_control_field, 8'b0, interrupt_field}
// PHY configuation register {24'b0, fsi_core_clock_divisor}

// NOTE: The FSI bus can be configured to run up to 166MHz according to the specification

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module fsi_master_litex_shim(
		// Configuration registers
		input wire [31:0] sys_clk_freq,

		// Wishbone slave port signals
		input wire slave_wb_cyc,
		input wire slave_wb_stb,
		input wire slave_wb_we,
		input wire [29:0] slave_wb_addr,
		input wire [31:0] slave_wb_dat_w,
		output wire [31:0] slave_wb_dat_r,
		input wire [3:0] slave_wb_sel,
		output wire slave_wb_ack,
		output wire slave_wb_err,

		output wire irq_o,

		output wire fsi_data_out,
		input wire fsi_data_in,
		output wire fsi_data_direction,			// 0 == tristate (input), 1 == driven (output)
		output wire fsi_clock_out,			// Must be inverted at the edge driver -- rising clocks are in reference to this signal, not the electrically inverted signal on the FSI bus

                input wire peripheral_reset,
                input wire peripheral_clock
	);

	// Control and status registers
	wire [63:0] device_id;
	wire [31:0] device_version;
	reg [31:0] sid_adr = 0;
	reg [31:0] control_reg = 0;
	wire [31:0] status_reg;
	reg [31:0] phy_cfg_reg = 32'h00000012;	// Default to a 1.5MHz FSI clock (divisor 18)

	reg [31:0] tx_data = 0;
	wire [31:0] rx_data;
	wire [31:0] dma_irq_reg;

	wire [7:0] fsi_core_clock_divisor;
	assign fsi_core_clock_divisor = phy_cfg_reg[7:0];

	// Device identifier
	assign device_id = 64'h7c5250544653494d;
	assign device_version = 32'h00010000;

	reg slave_wb_ack_reg = 0;
	reg [31:0] slave_wb_dat_r_reg = 0;

	assign slave_wb_ack = slave_wb_ack_reg;
	assign slave_wb_dat_r = slave_wb_dat_r_reg;

	parameter MMIO_TRANSFER_STATE_IDLE = 0;
	parameter MMIO_TRANSFER_STATE_TR01 = 1;

	reg [31:0] mmio_buffer_address_reg = 0;
	reg [7:0] mmio_transfer_state = 0;
	reg [31:0] mmio_cfg_space_tx_buffer = 0;
	reg [31:0] mmio_cfg_space_rx_buffer = 0;

	// Wishbone connector
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			// Reset Wishbone interface / control state machine
			slave_wb_ack_reg <= 0;

			mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
		end else begin
			case (mmio_transfer_state)
				MMIO_TRANSFER_STATE_IDLE: begin
					// Compute effective address
					mmio_buffer_address_reg[31:2] = slave_wb_addr;
					case (slave_wb_sel)
						4'b0001: mmio_buffer_address_reg[1:0] = 0;
						4'b0010: mmio_buffer_address_reg[1:0] = 1;
						4'b0100: mmio_buffer_address_reg[1:0] = 2;
						4'b1000: mmio_buffer_address_reg[1:0] = 3;
						4'b1111: mmio_buffer_address_reg[1:0] = 0;
						default: mmio_buffer_address_reg[1:0] = 0;
					endcase

					if (slave_wb_cyc && slave_wb_stb) begin
						// Configuration register space access
						// Single clock pulse signals in deasserted state...process incoming request!
						if (!slave_wb_we) begin
							// Read requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								0: mmio_cfg_space_tx_buffer = device_id[63:32];
								4: mmio_cfg_space_tx_buffer = device_id[31:0];
								8: mmio_cfg_space_tx_buffer = device_version;
								12: mmio_cfg_space_tx_buffer = sys_clk_freq;
								16: mmio_cfg_space_tx_buffer = sid_adr;
								20: mmio_cfg_space_tx_buffer = control_reg;
								24: mmio_cfg_space_tx_buffer = status_reg;
								28: mmio_cfg_space_tx_buffer = tx_data;
								32: mmio_cfg_space_tx_buffer = rx_data;
								36: mmio_cfg_space_tx_buffer = dma_irq_reg;
								40: mmio_cfg_space_tx_buffer = phy_cfg_reg;
								default: mmio_cfg_space_tx_buffer = 0;
							endcase

							// Endian swap
							slave_wb_dat_r_reg[31:24] <= mmio_cfg_space_tx_buffer[7:0];
							slave_wb_dat_r_reg[23:16] <= mmio_cfg_space_tx_buffer[15:8];
							slave_wb_dat_r_reg[15:8] <= mmio_cfg_space_tx_buffer[23:16];
							slave_wb_dat_r_reg[7:0] <= mmio_cfg_space_tx_buffer[31:24];

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end else begin
							// Write requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								// Device ID / version registers cannot be written, don't even try...
								16: mmio_cfg_space_rx_buffer = sid_adr;
								20: mmio_cfg_space_rx_buffer = control_reg;
								28: mmio_cfg_space_rx_buffer = tx_data;
								40: mmio_cfg_space_rx_buffer = phy_cfg_reg;
								// Status registers cannot be written, don't even try...
								default: mmio_cfg_space_rx_buffer = 0;
							endcase

							if (slave_wb_sel[0]) begin
								mmio_cfg_space_rx_buffer[7:0] = slave_wb_dat_w[31:24];
							end
							if (slave_wb_sel[1]) begin
								mmio_cfg_space_rx_buffer[15:8] = slave_wb_dat_w[23:16];
							end
							if (slave_wb_sel[2]) begin
								mmio_cfg_space_rx_buffer[23:16] = slave_wb_dat_w[15:8];
							end
							if (slave_wb_sel[3]) begin
								mmio_cfg_space_rx_buffer[31:24] = slave_wb_dat_w[7:0];
							end

							case ({mmio_buffer_address_reg[7:2], 2'b00})
								16: sid_adr <= mmio_cfg_space_rx_buffer;
								20: control_reg <= mmio_cfg_space_rx_buffer;
								28: tx_data <= mmio_cfg_space_rx_buffer;
								40: phy_cfg_reg <= mmio_cfg_space_rx_buffer;
							endcase

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end
					end
				end
				MMIO_TRANSFER_STATE_TR01: begin
					// Cycle complete
					slave_wb_ack_reg <= 0;
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
				default: begin
					// Should never reach this state
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
			endcase
		end
	end

	// FSI bus signals
	wire fsi_clock_internal;
	reg fsi_data_in_reg = 0;
	wire fsi_data_out_internal;
	reg fsi_data_out_reg = 0;
	wire fsi_data_direction_internal;
	reg fsi_data_direction_reg = 0;
	always @(posedge fsi_clock_internal) begin
		fsi_data_out_reg <= fsi_data_out_internal;
		fsi_data_direction_reg <= fsi_data_direction_internal;
		fsi_data_in_reg <= fsi_data_in;
	end
	assign fsi_data_out = fsi_data_out_reg;
	assign fsi_data_direction = fsi_data_direction_reg;
	assign fsi_clock_out = ~fsi_clock_internal;

	// PHY clock generator and reset synchronizer
	// Divisor:
	// (fsi_core_clock_divisor - 1) * 2
	// 0 == undefined (actually divide by two)
	// 1 == divide by 1
	// 2 == divide by 2
	// 3 == divide by 4
	// 4 == divide by 6
	// 5 == divide by 8
	// 6 == divide by 10
	// 7 == divide by 12
	// etc.
	(* noglobal *) reg fsi_master_core_reset = 0;
	wire fsi_master_core_clock;
	(* noglobal *) reg fsi_master_core_clock_gen_reg = 0;
	(* noglobal *) reg fsi_master_core_clock_gen_reg_prev = 0;
	reg [7:0] fsi_master_core_clock_counter = 0;
	assign fsi_master_core_clock = (fsi_core_clock_divisor == 1)?peripheral_clock:fsi_master_core_clock_gen_reg;
	always @(posedge peripheral_clock) begin
		// Clock generator
		if (fsi_master_core_clock_counter >= (fsi_core_clock_divisor - 2)) begin
			fsi_master_core_clock_gen_reg <= ~fsi_master_core_clock_gen_reg;
			fsi_master_core_clock_counter <= 0;
		end else begin
			fsi_master_core_clock_counter <= fsi_master_core_clock_counter + 1;
		end

		// Reset synchronizer
		if ((fsi_master_core_clock_gen_reg_prev == 0) && (fsi_master_core_clock_gen_reg == 1)) begin
			fsi_master_core_reset <= 0;
		end else begin
			if (peripheral_reset) begin
				fsi_master_core_reset <= 1;
			end
		end

		fsi_master_core_clock_gen_reg_prev <= fsi_master_core_clock_gen_reg;
	end

	wire [1:0] slave_id;
	wire [20:0] address;
	wire [1:0] data_length;
	wire data_direction;
	wire enable_irq;
	wire enable_relative_address;
	wire enable_extended_address_mode;
	wire enable_crc_protection;
	wire start_cycle;
	wire cycle_complete;
	wire [2:0] cycle_error;
	wire [1:0] enhanced_error_recovery;
	wire [7:0] internal_cmd_issue_delay;
	wire [3:0] ipoll_enable_slave_id;
	wire enable_ipoll;
	wire ipoll_error;
	wire [7:0] interrupt_field;
	wire [11:0] dma_control_field;
	wire [7:0] debug_port;

	fsi_master_interface U1(
		.slave_id(slave_id),
		.address(address),
		.tx_data(tx_data),
		.rx_data(rx_data),
		.data_length(data_length),
		.data_direction(data_direction),
		.enable_relative_address(enable_relative_address),
		.enable_extended_address_mode(enable_extended_address_mode),
		.enable_crc_protection(enable_crc_protection),
		.start_cycle(start_cycle),
		.cycle_complete(cycle_complete),
		.cycle_error(cycle_error),
		.enhanced_error_recovery(enhanced_error_recovery),
		.internal_cmd_issue_delay(internal_cmd_issue_delay),
		.ipoll_enable_slave_id(ipoll_enable_slave_id),
		.enable_ipoll(enable_ipoll),
		.ipoll_error(ipoll_error),
		.interrupt_field(interrupt_field),
		.dma_control_field(dma_control_field),

		.debug_port(debug_port),

		.fsi_data_out(fsi_data_out_internal),
		.fsi_data_in(fsi_data_in_reg),
		.fsi_data_direction(fsi_data_direction_internal),
		.fsi_clock_out(fsi_clock_internal),

		.peripheral_reset(fsi_master_core_reset),
		.peripheral_clock(fsi_master_core_clock)
	);

	// Map individual signals to external registers
	assign slave_id = sid_adr[30:29];
	assign address = sid_adr[28:8];
	assign data_length = sid_adr[1:0];
	assign data_direction = control_reg[1];
	assign enable_irq = control_reg[21];
	assign enable_relative_address = control_reg[20];
	assign enable_extended_address_mode = control_reg[19];
	assign enable_crc_protection = control_reg[18];
	assign start_cycle = control_reg[0];
	assign enhanced_error_recovery = control_reg[17:16];
	assign internal_cmd_issue_delay = control_reg[15:8];
	assign ipoll_enable_slave_id = control_reg[31:28];
	assign enable_ipoll = control_reg[24];
	assign status_reg = {debug_port, 8'b0, ipoll_error, 4'b0, cycle_error, 7'b0, cycle_complete};
	assign dma_irq_reg = {4'b0, dma_control_field, 8'b0, interrupt_field};

	// Handle IRQ assertions
	always @(posedge peripheral_clock) begin
		if (enable_irq) begin
			irq_o = (interrupt_field != 0);
		end else begin
			irq_o = 1'b0;
		end
	end
endmodule
