#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

setup(
    name="pythondata-peripheral-swiftfsi",
    description="Verilog files for Swift FSI module",
    author="Raptor Engineering, LLC",
    author_email="support@raptorengineering.com",
    url="https://www.raptorengineering.com",
    download_url="https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/pythondata-peripheral-swiftfsi",
    test_suite="test",
    license="LGPLv3+",
    python_requires="~=3.6",
    packages=find_packages(exclude=("test*", "sim*", "doc*", "examples*")),
    include_package_data=True,
)
